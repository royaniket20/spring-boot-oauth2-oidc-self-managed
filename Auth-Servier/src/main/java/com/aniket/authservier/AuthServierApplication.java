package com.aniket.authservier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;

@SpringBootApplication
public class AuthServierApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthServierApplication.class, args);
    }


}
